#!/bin/bash
echo "installing kernel headers"
sudo apt-get -y install linux-headers-$(uname -r)

CUDA_VERSION="8.0.44"
CUDA_PKG_VERSION="$CUDA_VERSION-3"
echo "installing cuda toolkit version " $CUDA_PKG_VERSION
sudo apt-get install nvidia-cuda-toolkit=$CUDA_PKG_VERSION
sudo apt-get -y install nvidia-cuda-doc=$CUDA_PKG_VERSION nvidia-cuda-gdb=$CUDA_PKG_VERSION nvidia-visual-profiler=$CUDA_PKG_VERSION
sudo apt-get -y install libcupti-dev=$CUDA_PKG_VERSION
