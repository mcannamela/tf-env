from __future__ import unicode_literals, print_function, division
import tensorflow as tf
import time
import numpy as np


def get_do_it_cpu():
    with tf.device('/cpu:0'):
        a = tf.constant(np.arange(4000000), shape=[200, 20000], name='a_cpu', dtype=tf.float32)
        b = tf.constant(np.arange(4000000), shape=[20000, 200], name='b_cpu', dtype=tf.float32)
        do_it = tf.matmul(a, b, name='doit_cpu')
        return do_it


def get_do_it_gpu():
    with tf.device('/gpu:0'):
        a = tf.constant(np.arange(4000000), shape=[200, 20000], name='a_gpu', dtype=tf.float32)
        b = tf.constant(np.arange(4000000), shape=[20000, 200], name='b_gpu', dtype=tf.float32)
        do_it = tf.matmul(a, b, name='doit_gpu')
        return do_it


if __name__ == "__main__":
    graph = tf.Graph()
    sess = tf.Session(graph=graph, config=tf.ConfigProto(log_device_placement=True))

    with graph.as_default():
        doit_cpu = get_do_it_cpu()
        doit_gpu = get_do_it_gpu()

        start = time.time()
        for i in range(100):
            if i == 0:
                print(sess.run(doit_cpu))
            else:
                sess.run(doit_cpu)

        elapsed_cpu = time.time() - start

        start = time.time()
        for i in range(100):
            if i == 0:
                print(sess.run(doit_gpu))
            else:
                sess.run(doit_gpu)

        elapsed_gpu = time.time() - start

        print('cpu: {}'.format(elapsed_cpu))
        print('gpu: {}'.format(elapsed_gpu))
