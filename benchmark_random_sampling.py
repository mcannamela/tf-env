from __future__ import unicode_literals, print_function, division
import tensorflow as tf
import time
import numpy as np
from scipy.stats import binom
from scipy.special import logit

_N_DIST = 1000
_N_TRIALS = np.array([100 + i*100 for i in range(_N_DIST)])
_RATES = np.linspace(1e-6, 1.0-1e-6, _N_DIST)
_SAMPLE_SHAPE = (1,)
# _SAMPLE_SHAPE = (10,)
# _SAMPLE_SHAPE = (10, 10)


def get_do_it_cpu(session):
    raise NotImplementedError()
    with tf.device('/cpu:0'):
        d = [tf.contrib.distributions.Binomial(tf.to_float(n), probs=tf.to_float([p, 1.0-p]), name="multinomial_cpu") for n, p in zip(_N_TRIALS, _RATES)]
        # s = [tf.expand_dims(x.sample(_SAMPLE_SHAPE), 0) for i, x in enumerate(d)]
        # do_it_op = tf.concat(s, axis=0)

        def do_it():
            return session.run(do_it_op)

    return do_it


def get_do_it_gpu(session):
    raise NotImplementedError()
    with tf.device('/gpu:0'):
        d = [tf.contrib.distributions.Multinomial(tf.to_float(n), probs=tf.to_float([p, 1.0 - p]),
                                                  name="multinomial_gpu") for n, p in zip(_N_TRIALS, _RATES)]
        s = [tf.expand_dims(x.sample(_SAMPLE_SHAPE), 0) for i, x in enumerate(d)]
        do_it_op = tf.concat(s, axis=0)

        def do_it():
            return session.run(do_it_op)

    return do_it


def get_do_it_scipy():
    def do_it():
        d = binom(_N_TRIALS, _RATES)
        return d.rvs(size=_SAMPLE_SHAPE + (_N_DIST,))

    return do_it


def get_do_it_scipy_dumb():
    def do_it():
        d = [binom(n, r) for n, r in zip(_N_TRIALS, _RATES)]
        return [x.rvs(size=_SAMPLE_SHAPE) for x in d]

    return do_it


def get_do_it_scipy_really_dumb():
    def do_it():
        d = [binom(n, r) for n, r in zip(_N_TRIALS, _RATES)]
        return [x.rvs(size=_SAMPLE_SHAPE) for x in d]

    return do_it


def get_do_it_numpy():
    def do_it():
        return np.random.binomial(_N_TRIALS, _RATES, _SAMPLE_SHAPE + (_N_DIST,))

    return do_it


def get_do_it_numpy_dumb():
    def do_it():
        return [np.random.binomial(n, r, _SAMPLE_SHAPE) for n, r in zip(_N_TRIALS, _RATES)]

    return do_it


def time_f(f, n_tests):
    start = time.time()
    for i in range(n_tests):
        s = f()
        if i == 0:
            if isinstance(s, list):
                print(len(s), s[0].shape)
            else:
                print(s.shape)

    elapsed = time.time() - start
    return elapsed

if __name__ == "__main__":
    # Creates a session with log_device_placement set to True.
    graph = tf.Graph()
    sess = tf.Session(graph=graph, config=tf.ConfigProto(log_device_placement=False))

    with graph.as_default():
        # doit_cpu = get_do_it_cpu(sess)
        # doit_gpu = get_do_it_gpu(sess)
        doit_scipy = get_do_it_scipy()
        doit_scipy_dumb = get_do_it_scipy_dumb()
        doit_scipy_really_dumb = get_do_it_scipy_really_dumb()
        doit_numpy = get_do_it_numpy()
        doit_numpy_dumb = get_do_it_numpy_dumb()

        n_tests = 1

        elapsed_cpu = np.inf
        elapsed_gpu = np.inf
        elapsed_scipy = np.inf
        elapsed_scipy_dumb = np.inf
        elapsed_numpy = np.inf
        elapsed_numpy_dumb = np.inf

        elapsed_cpu = time_f(doit_cpu, n_tests)
        # elapsed_gpu = time_f(doit_gpu, n_tests)
        elapsed_scipy = time_f(doit_scipy, n_tests)
        elapsed_scipy_dumb = time_f(doit_scipy_dumb, n_tests)
        elapsed_scipy_reallydumb = time_f(doit_scipy_really_dumb, n_tests)
        elapsed_numpy = time_f(doit_numpy, n_tests)
        elapsed_numpy_dumb = time_f(doit_numpy_dumb, n_tests)

        print('cpu: {}'.format(elapsed_cpu))
        print('gpu: {}'.format(elapsed_gpu))
        print('scipy: {}'.format(elapsed_scipy))
        print('scipy_dumb: {}'.format(elapsed_scipy_dumb))
        print('scipy_really_dumb: {}'.format(elapsed_scipy_dumb))
        print('numpy: {}'.format(elapsed_numpy))
        print('numpy_dumb: {}'.format(elapsed_numpy_dumb))

