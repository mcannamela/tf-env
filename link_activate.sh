ENV_NAME="tf-venv"
ENV_PATH=$HOME/$ENV_NAME

if [ "$1" != "" ]; then
    echo "Linking activation script of $ENV_PATH to $1"
else
    echo "Specify location to link "
    exit 1
fi

ln -s $ENV_PATH/bin/activate $1/activate