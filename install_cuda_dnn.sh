#!/bin/bash
echo "installing cuda dnn"


CUDA_DNN_PATH=/usr/local/include/cudnn
if [ "$1" != "" ]; then
    echo "Installing cu dnn to $CUDA_DNN_PATH from $1"
else
    echo "Specify location of cudnn "
    exit 1
fi

sudo cp -r $1 $CUDA_DNN_PATH

echo "add this to your bashrc"

echo "export LD_LIBRARY_PATH=$CUDA_DNN_PATH/include:$CUDA_DNN_PATH/lib64:\$LD_LIBRARY_PATH"