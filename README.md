# README #

This repo provides environments for tensorflow that include GPU support. Installing CUDA can be fraught, so the main purpose is to ensure we have a consistent process for doing this.

### Quickstart ###
We need CUDA 8.0x as of this writing.
If you have previously installed a (wrong) version of CUDA, first uninstall it:
	remove_cuda.sh

Now install the toolkit itself:
	install_cuda.sh

We also need cuDNN 5.1. Unfortunately, you just have to go forth and download it from Nvidia's website. 
Done that? 
	install_cuda_dnn.sh /path/to/downloaded/cudnn

Be sure to adjust your path as the script directs.

Finally, we can build the virtualenv:
	setup_env.sh

This will put the virtualenv in ~/tf-venv, and make a link to the activation script in this repo. 

### Testing ###
A restart may be required after installing all the things to get tensorflow and cuda playing nicely.

Verify you can compute things on the GPU with tensorflow: 
	source ./activate
	python hello_tf_gpu.py

Verify you can see plots:
	source ./activate
	python prove_plot.py

### Deployment ###
For the very lazy, 
	link_activate.sh /path/to/other/repo

will put a shortcut someplace else, so you don't have to refer to the full env path all the time.