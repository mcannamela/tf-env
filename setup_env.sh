#!/bin/bash

echo "uninstalling potential troublemakers from apt, which must be pip installed later. Must sudo:"
sudo apt -y remove python-numpy python-six python-mock matplotlib

echo "installing system dependencies, must sudo:"
sudo apt -y install python-traits libfreetype6-dev python-tk libblas3 libjs-jquery liblapack3  build-essential python-dev python-tk


echo "ensure we have the most up to date pip and virtualenv"
sudo apt-get remove python-pip
curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"
sudo -H python get-pip.py
rm ./get-pip.py

sudo -H pip install virtualenv

ENV_NAME="tf-venv"
ENV_PATH=$HOME/$ENV_NAME

echo "env path is " $HOME

if [ -d $ENV_PATH ]; then
  echo "removing existing virtualenv at " $ENV_PATH
  sudo rm -rf $ENV_PATH;
fi


echo "creating and activating virtualenv"
virtualenv $ENV_PATH
source $ENV_PATH/bin/activate


ln -s $ENV_PATH/bin/activate ./activate
echo "we've linked the activation script to the current directory" 

echo "stupid virtualenv itself behind in pip, upgrading"
pip install --upgrade pip


echo "installing python requirements"
pip install -r requirements.txt

echo "all set. activate the env and try running hello_tf_gpu.py to make sure it all works"
